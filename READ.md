1. Опишіть своїми словами що таке Document Object Model (DOM)

Він представляє штмл документ у вигляді дерева

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML повертає або задає HTML-код всередині елемента, innerText повертає або задає текстовий вміст без урахування HTML 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

getElementById, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll

4. Яка різниця між nodeList та HTMLCollection?

NodeList - це колекція вузлів, які можуть бути будь-якого типу вузла DOM
HTMLCollection - це специфічна для HTML колекція, яка містить лише елементи HTML-документа
